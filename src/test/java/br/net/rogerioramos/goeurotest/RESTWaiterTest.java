package br.net.rogerioramos.goeurotest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.ws.rs.BadRequestException;

import org.junit.Before;
import org.junit.Test;

import br.net.rogerioramos.goeurotest.RESTWaiter;
import br.net.rogerioramos.goeurotest.Result;

public class RESTWaiterTest {
    private RESTWaiter waiter = null;

    @Before
    public void setUp() { }

    @Test
    public void testFindString() {
	waiter = new RESTWaiter();
	List<Result> results = waiter.find("São Paulo");

	assertTrue(null != results);
	assertTrue(results instanceof List<?>);
	assertEquals(5, results.size());

	Result first = results.get(0);

	assertTrue(first instanceof Result);
	assertEquals(371772, first.getId());
	assertEquals("São Paulo", first.getName());
	assertEquals("location", first.getType());
	assertEquals(-23.5475f, first.getPosition().getLatitude(), 0);
	assertEquals(-46.63611f, first.getPosition().getLongitude(), 0);
    }

    @Test
    public void testFind() {
	waiter = new RESTWaiter("Berlin");
	List<Result> results = waiter.find();

	assertTrue(null != results);
	assertTrue(results instanceof List<?>);
	assertEquals(8, results.size());

	Result second = results.get(1);

	assertTrue(second instanceof Result);
	assertEquals(448103, second.getId());
	assertEquals("Berlingo", second.getName());
	assertEquals("location", second.getType());
	assertEquals(45.50298f, second.getPosition().getLatitude(), 0);
	assertEquals(10.04366f, second.getPosition().getLongitude(), 0);
    }

    @Test
    public void testNullCriteria() {
	waiter = new RESTWaiter(null);
	List<Result> results = waiter.find();

	assertTrue(results.size() == 0);
    }

    @Test(expected = BadRequestException.class)
    public void testEmptyCriteria() {
	waiter = new RESTWaiter("");
	List<Result> results = waiter.find();

	assertTrue(results.size() == 0);
    }

    @Test
    public void testDashCriteria() {
	waiter = new RESTWaiter("-");
	List<Result> results = waiter.find();

	assertTrue("There is results with dash", results.size() > 0);
    }

    @Test
    public void testPercentCriteria() {
	waiter = new RESTWaiter("%");
	List<Result> results = waiter.find();

	assertTrue(results.size() == 0);
    }

    @Test
    public void testSingleQuoteCriteria() {
	waiter = new RESTWaiter("'");
	List<Result> results = waiter.find();

	assertTrue("There is results with dash", results.size() > 0);
    }

    @Test
    public void testHashCriteria() {
	waiter = new RESTWaiter("#");
	List<Result> results = waiter.find();

	assertTrue(results.size() == 0);
    }
    
    @Test
    public void testCrazyCriteria() {
	waiter = new RESTWaiter("AJGHGA%&%$%%&&%&*6868564646546´´´´´´¨¨¨¨΅'''''\"\"\"\"ieurioew0982347234");
	List<Result> results = waiter.find();

	assertTrue(results.size() == 0);
    }
}
package br.net.rogerioramos.goeurotest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.net.rogerioramos.goeurotest.JSONWaiter;
import br.net.rogerioramos.goeurotest.Result;

public class JSONWaiterTest {
    private static final String testResourcesPath = 
	    "src/test/resources/br/net/rogerioramos/goeurotest/sao_paulo_result.json";
    private JSONWaiter waiter;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void fromJSONTest() {
	String response = getFixturesJSONFile();
	waiter = new JSONWaiter();
	waiter.setResponse(response);

	List<Result> results  = waiter.fromJSON();
	Result first = results.get(0);

	assertTrue(results instanceof List<?>);
	assertEquals(5, results.size());
	assertTrue(first instanceof Result);
	assertEquals(371772, first.getId());
	assertEquals("São Paulo", first.getName());
	assertEquals("location", first.getType());
	assertEquals(-23.5475f, first.getPosition().getLatitude(), 0);
	assertEquals(-46.63611f, first.getPosition().getLongitude(), 0);
    }

    @Test
    public void fromJSONStringTest() {
	String response = getFixturesJSONFile();
	waiter = new JSONWaiter(response);

	List<Result> results  = waiter.fromJSON();
	Result second = results.get(1);

	assertTrue(results instanceof List<?>);
	assertEquals(5, results.size());
	assertTrue(second instanceof Result);
	assertEquals(371770, second.getId());
	assertEquals("São Paulo de Olivença", second.getName());
	assertEquals("location", second.getType());
	assertEquals(-3.37833f, second.getPosition().getLatitude(), 0);
	assertEquals(-68.8725f, second.getPosition().getLongitude(), 0);
    }

    private String getFixturesJSONFile() {
	String content = null;
	File f = new File(testResourcesPath);

	try {
	    content = new String(Files.readAllBytes(
		    Paths.get(f.getAbsolutePath())));

	} catch (IOException e) {
	    fail("Error reading the fixture json file: " + e.getMessage());

	} finally {
	    f = null;
	}

	return content;
    }
}
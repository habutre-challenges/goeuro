package br.net.rogerioramos.goeurotest;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.net.rogerioramos.goeurotest.CSVWaiter;
import br.net.rogerioramos.goeurotest.JSONWaiter;
import br.net.rogerioramos.goeurotest.Result;

public class CSVWaiterTest {

    private static ByteArrayOutputStream sysout = 
	    new ByteArrayOutputStream();
    private static final String testResourcesPath = 
	    "src/test/resources/br/net/rogerioramos/goeurotest/sao_paulo_result.json";
    private static final CharSequence PART_1_MSG = "The file";
    private static final CharSequence PART_2_MSG = "was created at";

    @Before
    public void setUp() throws Exception {
	System.setOut(new PrintStream(sysout));
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testDeliver() {
	List<Result> results = new JSONWaiter().fromJSON(getFixturesJSONFile());
	CSVWaiter csv = new CSVWaiter(results);
	
	try {
	    csv.deliver();

	    assertTrue("A message should be shown to end-user", sysout
		    .toString().contains(PART_1_MSG));
	    assertTrue("A message should be shown to end-user", sysout
		    .toString().contains(PART_2_MSG));

	} catch (IOException e) {
	    fail("Something went wrong: " + e.getMessage());
	}
    }

    private String getFixturesJSONFile() {
	String content = null;
	File f = new File(testResourcesPath);

	try {
	    content = new String(Files.readAllBytes(
		    Paths.get(f.getAbsolutePath())));

	} catch (IOException e) {
	    fail("Error reading the fixture json file: " + e.getMessage());

	} finally {
	    f = null;
	}

	return content;
    }
}

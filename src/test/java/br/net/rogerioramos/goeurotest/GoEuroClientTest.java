package br.net.rogerioramos.goeurotest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.net.rogerioramos.goeurotest.GoEuroClient;

public class GoEuroClientTest {

    private static ByteArrayOutputStream sysout = 
	    new ByteArrayOutputStream();
    private static ByteArrayOutputStream syserr = 
	    new ByteArrayOutputStream();
    private final static String PARTIAL_EXPECTED_OUT = 
	    "The expected argument was not informed";
    private final static String EXPECTED_NULL_ARGS_MSG = 
	    "Invalid argument";
    private final static String EXPECTED_COUNT_ARGS_MSG = 
	    "Invalid argument count";
    private final static String EXPECTED_EMPTY_ARGS_MSG = 
	    "Invalid argument value";

    @BeforeClass
    public static void initialSetUp() {

    }

    @Before
    public void setUp() {
	System.setOut(new PrintStream(sysout));
	System.setErr(new PrintStream(syserr));
    }

    @After
    public void setDown(){
	syserr = new ByteArrayOutputStream();
	sysout = new ByteArrayOutputStream();
    }

    public void testMain() {
	String[] args = {"Berlin"};

	try {
	    GoEuroClient.main(args);

	} catch (IOException e) {
	    fail("Unexpected exception");
	}
	assertFalse(syserr.toString().contains(PARTIAL_EXPECTED_OUT));
	assertFalse(syserr.toString().contains(EXPECTED_NULL_ARGS_MSG));
	assertFalse((syserr.toString().contains(EXPECTED_COUNT_ARGS_MSG)));
	assertFalse(syserr.toString().contains(EXPECTED_EMPTY_ARGS_MSG));
    }

    @Test
    public void testMainWithNullArgs() {
	String[] args = null;

	try {
	    GoEuroClient.main(args);

	} catch (IOException e) {
	    fail("Unexpected exception");
	}
	assertTrue(syserr.toString().contains(PARTIAL_EXPECTED_OUT));
	assertTrue(syserr.toString().contains(EXPECTED_NULL_ARGS_MSG));
	assertFalse((syserr.toString().contains(EXPECTED_COUNT_ARGS_MSG)));
	assertFalse(syserr.toString().contains(EXPECTED_EMPTY_ARGS_MSG));
    }

    @Test
    public void testMainWithEmptyArgs() {
	String[] args = {""};

	try {
	    GoEuroClient.main(args);

	} catch (IOException e) {
	    fail("Unexpected exception");
	}
	assertTrue(syserr.toString().contains(PARTIAL_EXPECTED_OUT));
	assertTrue(syserr.toString().contains(EXPECTED_NULL_ARGS_MSG));
	assertTrue(syserr.toString().contains(EXPECTED_EMPTY_ARGS_MSG));
	assertFalse((syserr.toString().contains(EXPECTED_COUNT_ARGS_MSG)));
    }

    @Test
    public void testMainWithInvalidCountArgs() {
	String[] args = {"A", "B"};

	try {
	    GoEuroClient.main(args);

	} catch (IOException e) {
	    fail("Unexpected exception");
	}
	assertTrue(syserr.toString().contains(PARTIAL_EXPECTED_OUT));
	assertTrue(syserr.toString().contains(EXPECTED_NULL_ARGS_MSG));
	assertTrue((syserr.toString().contains(EXPECTED_COUNT_ARGS_MSG)));
	assertFalse(syserr.toString().contains(EXPECTED_EMPTY_ARGS_MSG));
    }

    @Test
    public void testMainWithNoArgs() {
	String[] args = {};

	try {
	    GoEuroClient.main(args);

	} catch (IOException e) {
	    fail("Unexpected exception");
	}
	assertTrue(syserr.toString().contains(PARTIAL_EXPECTED_OUT));
	assertTrue(syserr.toString().contains(EXPECTED_NULL_ARGS_MSG)); 
	assertTrue(syserr.toString().contains(EXPECTED_COUNT_ARGS_MSG));
	assertFalse(syserr.toString().contains(EXPECTED_EMPTY_ARGS_MSG));
    }
}
package br.net.rogerioramos.goeurotest;

public class ResultPosition {

    private float latitude;
    private float longitude;

    public ResultPosition() {
    }

    public ResultPosition(float latitude, float longitude) {
	this.setLatitude(latitude);
	this.setLongitude(longitude);
    }

    public ResultPosition(String latitude, String longitude) {
	this.setLatitude(latitude);
	this.setLongitude(longitude);
    }

    public float getLatitude() {
	return latitude;
    }

    public void setLatitude(float latitude) {
	this.latitude = latitude;
    }

    private void setLatitude(String latitude) {
	this.setLatitude(Float.parseFloat(latitude));
    }

    public float getLongitude() {
	return longitude;
    }

    public void setLongitude(float longitude) {
	this.longitude = longitude;
    }

    private void setLongitude(String longitude) {
	this.setLongitude(Float.parseFloat(longitude));
    }
}
package br.net.rogerioramos.goeurotest;

import java.io.IOException;
import java.util.List;

public class GoEuroClient {

    private final static boolean isArgsValid(String[] args) {	
	if (null == args) {
	    error("Invalid argument");
	    return false;
	}

	if (args.length != 1) {
	    error("Invalid argument count");
	    return false;
	}

	if (args[0].trim().isEmpty()) {
	    error("Invalid argument value");
	    return false;
	}

	return true;
    }

    private final static void error(String msg) {
	System.err.println(msg);
    }
    
    private final static void say(String msg) {
	System.out.println(msg);
    }

    public static void main(String[] args) throws IOException {
	RESTWaiter client = null;
	
	if (!isArgsValid(args)) {
	    error("The expected argument was not informed. Please inform the location to be researched");
	    return;
	}

	client = new RESTWaiter(args[0]);
	List<Result> result = client.find();
	
	if (null != result && result.size() > 0) {
	    CSVWaiter csv = new CSVWaiter(result);
	    csv.deliver();
	    System.exit(0);
	}

	say("Your search ended up with no results!");
    }

    

}
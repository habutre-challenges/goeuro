package br.net.rogerioramos.goeurotest;

import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

public class RESTWaiter {

    private static final String ENDPOINT = "http://api.goeuro.com";
    private static final String API_V2 = "/api/v2";
    private static final String RESOURCE = "/position/suggest/en/";
    private String criteria;

    public RESTWaiter() {
    }

    public RESTWaiter(String criteria) {
	this.setCriteria(criteria);
    }

    public List<Result> find() {
	JSONWaiter waiter = new JSONWaiter();
	String response = requestLocation();

	return waiter.fromJSON(response);
    }

    public List<Result> find(String city) {
	this.setCriteria(city);
	return this.find();
    }

    private String requestLocation() {
	ClientConfig config = new ClientConfig();
	Client restClient = ClientBuilder.newClient(config);

	Builder request = restClient.target(ENDPOINT).path(API_V2 + RESOURCE + this.getCriteria())
		.request(MediaType.APPLICATION_JSON);
	Response response = request.get();

	if (response.getStatus() != 200)
	    throw new BadRequestException();

	return response.readEntity(String.class);
    }

    public String getCriteria() {
	return criteria;
    }

    public void setCriteria(String criteria) {
	this.criteria = criteria;
    }
}
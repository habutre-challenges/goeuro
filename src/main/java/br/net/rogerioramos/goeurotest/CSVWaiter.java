package br.net.rogerioramos.goeurotest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

public class CSVWaiter {

    private static final String DELIMITER = ";";
    private static final String CVS_NAME = "go_euro_challenge";
    private List<Result> results;

    public CSVWaiter() {
    }

    public CSVWaiter(List<Result> resultsFound) {
	this.setResults(resultsFound);
    }

    public boolean deliver() throws IOException {

	if (null == this.getResults())
	    throw new RuntimeException("The expected results had not been sent");

	File f = this.writeFile(this.csvStream());

	if (f.exists()) {
	    System.out.println("The file " + f.getName() + 
		    " was created at " + f.getPath());
	    return true;
	}

	return false;
    }

    private String csvStream() {
	StringBuilder sb = new StringBuilder();

	for (Result result : results) {
	    sb.append(result.getId());
	    sb.append(DELIMITER);
	    sb.append(result.getName());
	    sb.append(DELIMITER);
	    sb.append(result.getType());
	    sb.append(DELIMITER);
	    sb.append(result.getPosition().getLatitude());
	    sb.append(DELIMITER);
	    sb.append(result.getPosition().getLongitude());
	    sb.append(System.lineSeparator());
	}

	return sb.toString();
    }

    public File writeFile(String content) throws IOException {
	File f = new File(System.getProperty("java.io.tmpdir") + File.separator +
		CVS_NAME + "_" + this.currentTime() + ".csv");

	FileWriter fw = new FileWriter(f);
	fw.write(content);
	fw.flush();
	fw.close();

	return f;
    }

    private String currentTime() {
	Calendar cal = Calendar.getInstance();

	return String.valueOf(cal.get(Calendar.YEAR)) + String.valueOf(cal.get(Calendar.MONTH))
		+ String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
    }

    public List<Result> getResults() {
	return results;
    }

    public void setResults(List<Result> results) {
	this.results = results;
    }
}

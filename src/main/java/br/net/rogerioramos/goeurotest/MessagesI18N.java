package br.net.rogerioramos.goeurotest;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class MessagesI18N extends ResourceBundle {
    private final String MESSAGE_FILE = "resources/messages";
    
    private static ResourceBundle bundle;
    
    public MessagesI18N() {
	bundle = ResourceBundle.getBundle(MESSAGE_FILE);
    }
    
    public MessagesI18N(Locale l) {
	bundle = ResourceBundle.getBundle(MESSAGE_FILE, l);
    }

    public static String getMessage(String key){
	return bundle.getString(key);
    }
    
    @Override
    public Enumeration<String> getKeys() {
	return Collections.enumeration(keySet());
    }

    @Override
    protected Object handleGetObject(String key) {
	// TODO Auto-generated method stub
	return null;
    }
   
}

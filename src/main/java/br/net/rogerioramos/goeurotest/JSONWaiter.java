package br.net.rogerioramos.goeurotest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class JSONWaiter {
    private String response;
    
    public JSONWaiter() {}
    
    public JSONWaiter(String response) {
	this.setResponse(response);
    }
    
    public List<Result> fromJSON() {
	Type listResult = new TypeToken<ArrayList<Result>>() {}.getType();
	ArrayList<Result> results = null;
	
	Gson marshall = new GsonBuilder().create();
	results = marshall.fromJson(this.getResponse(), listResult);
	
	return	results;
    }
    
    public List<Result> fromJSON(String response) {
	this.setResponse(response);
	return fromJSON();
    }

    public String getResponse() {
	return response;
    }

    public void setResponse(String response) {
	this.response = response;
    }

}
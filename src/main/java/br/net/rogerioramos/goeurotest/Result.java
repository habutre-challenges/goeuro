package br.net.rogerioramos.goeurotest;

public class Result {
    private int _id;
    private String name;
    private String type;
    private ResultPosition geo_position;

    public Result() {
    }

    public Result(String id, String name, String type, String latitude, String longitude) {
	this.setId(id);
	this.setName(name);
	this.setType(type);
	this.setPostion(latitude, longitude);
    }

    public Result(int id, String name, String type, float latitude, float longitude) {
	this.setId(id);
	this.setName(name);
	this.setType(type);
	this.setPostion(latitude, longitude);
    }

    public int getId() {
	return _id;
    }

    public void setId(int id) {
	this._id = id;
    }

    private void setId(String id) {
	this.setId(Integer.parseInt(id));
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public ResultPosition getPosition() {
	if (null == this.geo_position)
	    this.geo_position = new ResultPosition();
	
	return geo_position;
    }

    public void setPosition(ResultPosition position) {
	this.geo_position = position;
    }

    public void setPostion(float latitude, float longitude) {
	this.setPosition(new ResultPosition(latitude, longitude));
    }

    public void setPostion(String latitude, String longitude) {
	this.setPosition(new ResultPosition(latitude, longitude));
    }

    public boolean isValid() {
	return null != this.getPosition() &&
		this.getId() > 0 &&
		null != this.getName() &&
		null != this.getType();
    }

    public boolean equals(Object o) {
	if (o instanceof Result && ((Result) o).getId() == this.getId()) {
	    return true;
	}

	return false;
    }

}